package com.pinpointer.andrew.pinpointer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    // Go to the sign up page when the 'Sign Up' button is tapped
    public void onSignUpButtonClick(View view) {
            startActivity(new Intent(this, SignUpActivity.class));
    }

    // Go to the main page (containing a list with freelancers) when the 'Login' button is tapped
    public void onLoginClick(View view){
        startActivity(new Intent(this, MainActivity.class));
    }
}
