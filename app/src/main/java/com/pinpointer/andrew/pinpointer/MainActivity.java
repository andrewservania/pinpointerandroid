package com.pinpointer.andrew.pinpointer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.UserManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    // list of freelancers

    ArrayList<PinPointerUser> users;

    //ListViewAdapter listViewAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

         users = new ArrayList<PinPointerUser>();
        users.add(new PinPointerUser(0,"Andrew", "Servania", "Computer Repair Guy", R.drawable.service_provider1));
//        users.add(new PinPointerUser(3,"Jonathan", "Halvik", "Carpenter", R.drawable.service_provider5));
//        users.add(new PinPointerUser(4,"Bernard", "Opertens", "Bike fixer", R.drawable.service_provider2));
//        users.add(new PinPointerUser(5,"Max", "Arvent", "Pastry baker", R.drawable.service_provider2));
//        users.add( new PinPointerUser(6,"Rasmus", "Quers", "Embroider", R.drawable.service_provider4));
//        users.add( new PinPointerUser(7,"Herven", "Endizos", "Tattooist", R.drawable.service_provider6));
//        users.add( new PinPointerUser(8,"Carlton", "Urvenska", "English Tutor", R.drawable.service_provider1));
//        users.add( new PinPointerUser(9,"Jason", "Erventinasindo", "Hair dresser", R.drawable.service_provider2));
//        users.add( new PinPointerUser(10,"Samantha", "Gertos", "Mechanic", R.drawable.service_provider5 ));
//        users.add( new PinPointerUser(11,"Alicia", "Halvore", "Clock fixer", R.drawable.service_provider3));

        //GetFreelancers(this);
        users.add(  new PinPointerUser(1,"Herbert", "Andersson", "Cake Baker", R.drawable.service_provider2));
        users.add(new PinPointerUser(2,"Julia", "Vernes", "Taxi driver", R.drawable.service_provider3));


        ListViewAdapter listViewAdapter = new ListViewAdapter(this, users);
        listViewAdapter.setPinPointerUsers(users);

        final ListView freelancers = (ListView) findViewById(R.id.freelancerList);

        // Do stuff with an item when clicked upon.
        // In this case, go to the User page.
        freelancers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                 GoToUserPage();
            }
        });
        freelancers.setAdapter(listViewAdapter);
    }


    private void  GoToUserPage()
    {
        Intent intent = new Intent(this, UserMainActivity.class);
        startActivity(intent);
    }

    private ArrayList<PinPointerUser> GetFreelancers(final Context context)
    {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://192.168.2.6:3011/Requests/GetFreelancers";
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try{

                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    JsonParser  parser = new JsonParser();
                    JsonArray array = parser.parse(response).getAsJsonArray();

                    //users.clear();
                    //users.add( new PinPointerUser(11,"Alicia", "Halvore", "Clock fixer", R.drawable.service_provider3));


                    ListViewAdapter listViewAdapter = new ListViewAdapter(context, users);
                    int tempUserId = users.size();

                    for(final JsonElement json: array){
                        //Issue: Not all retrieved elements are displayed correctly on the listview after logging in.
                        PinPointerUser entity = gson.fromJson(json, PinPointerUser.class);

                        switch (tempUserId)
                        {
                            case 0:entity.ImageID = R.drawable.service_provider1; break;
                            case 1:entity.ImageID = R.drawable.service_provider2; break;
                            case 2:entity.ImageID = R.drawable.service_provider3; break;
                            default: entity.ImageID = R.drawable.service_provider6; break;
                        }


                        entity.ID = tempUserId;
                        listViewAdapter.AddPinPointerUser(new PinPointerUser(
                                entity.ID,
                                entity.FirstName,
                                entity.LastName,
                                entity.Occupation,
                                entity.ImageID
                        ));

                        tempUserId++;
                    }

                    final ListView freelancers = (ListView) findViewById(R.id.freelancerList);

                    freelancers.setAdapter(listViewAdapter);

                } catch (Exception ex){

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
        return users;
    }

}
