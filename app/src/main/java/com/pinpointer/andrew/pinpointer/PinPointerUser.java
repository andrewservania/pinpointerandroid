package com.pinpointer.andrew.pinpointer;

/**
 * Created by Andrew on 12/1/2015.
 *
 * Model class used to identify a PinPointer user
 */
public class PinPointerUser {

    public int ID;

    public String InstanceIdentifier;

    public String FirstName;

    public String LastName;

    public String Occupation;

    public float latitude;

    public float longitude;

    // how close this particular user is from 'you'
    public float distance;

    public int ImageID;

    public PinPointerUser(int id, String firstName, String lastName, String occupation, int imageID) {

        this.ID = id;
        this.FirstName = firstName;
        this.LastName = lastName;
        this.Occupation = occupation;
        this.ImageID = imageID;
    }

    public long getUserID()
    {
        return ID;
    }

}
