package com.pinpointer.andrew.pinpointer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Andrew on 11/30/2015.
 *
 * Custom PinPointer freelancers item list
 */
public class ListViewAdapter extends BaseAdapter {

    Context context;
    private static LayoutInflater inflater = null;
    private ArrayList<PinPointerUser> users;

    public ListViewAdapter(Context context, ArrayList<PinPointerUser> users)
    {
        this.context = context;

        this.users = users;

        // TODO: what does this line do? research!
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public void setPinPointerUsers(ArrayList<PinPointerUser> _users)
    {
        users = _users;
        //this.notifyDataSetChanged();
    }

    public void AddPinPointerUser(PinPointerUser user)
    {
        users.add(user);
        //this.notifyDataSetChanged();
    }

    public void ClearList()
    {
        users.clear();
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return users.get(position).getUserID();
    }

    // Is called when list view is created. Make put everything in the list you want to display here!
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null)
            {
                view = inflater.inflate(R.layout.list_item,null);
            }
        else {

            String fullName = users.get(position).FirstName + " " + users.get(position).LastName;
            int imageID = users.get(position).ImageID;
            String occupation = users.get(position).Occupation;

            ImageView freelancerPicture = (ImageView) view.findViewById(R.id.freelancerPicture);
            TextView freelancerName = (TextView) view.findViewById(R.id.freelancerName);
            TextView freelancerOccupation = (TextView) view.findViewById(R.id.freelancerOccupation);

            // Set freelancer picture
            freelancerPicture.setBackgroundResource(imageID);
            //Set freelancer name
            freelancerName.setText(fullName);
            // Set freelancer occupation
            freelancerOccupation.setText(occupation);
        }

        return view;
    }


}
